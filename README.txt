
Module: AJAX Session
Author: Shawn Conn <www.shawnconn.com/contact>
Version: 1.0

Description
===========
Enables a user to set session variables using a javascript function that the 
module inserts into pages.


Installation
============
* Copy the 'ajax_session' to module dir & enable per usual Drupal modules.

Usage
=====
In order to prevent unscrupulous users from doing anything malicious the only 
session variables that can be set through AJAX session are ones registered with 
AJAX session. This is done by calling the PHP function:

ajax_session_register($name, $retain=FALSE)

Where $name is the session variable name to register. If $retain is set to TRUE 
this variable will be stored across registered user sessions. By default, the 
permission to use AJAX session is set to denied. After enabling the permission
for specified roles and registering session variables, you can use the 
javascript function: 

ajax_session_send_variable(name, value) 

Upon using this function a user will have the session variable name altered to 
value with the next page request. If this variable is set to be retained across 
registered user sessions, it will be stored when the user logs out and set when 
the user logs back in.