<?php
// $Id: ajax_session.module

/**
 * @file
 * Enables menu callback that enables storage of session variables through AJAX
 */

/**
 * implementation of hook_init
 * Sets session variables that were set through AJAX session.
 */
function ajax_session_init() {
	if ($cache = cache_get('ajax_session_'._get_session_id())) {
		foreach(unserialize($cache->data) as $key => $value) {
			$_SESSION[$key] = $value;
		}
		cache_clear_all('ajax_session_'._get_session_id(),'cache');
	}
}

/**
 * implementation of hook_user
 * Allows session variables to persist through different sessions
 */
function ajax_session_user($op, $edit, &$account, $category = NULL)  {
	$retained = variable_get('ajax_session_retained',array());
	switch ($op) {
		case 'login':
			if ($cache = cache_get('ajax_session_retain_'.$account->uid)) {
				foreach(unserialize($cache->data) as $key => $value) {
					$_SESSION[$key] = $value;
				}
				cache_clear_all('ajax_session_retain_'.$account->uid,'cache');
			}
			break;
		case 'logout':
			$retained_variables = array();
			foreach ($_SESSION as $name => $value) {
				if (in_array($name,$retained)) {
					$retained_variables[$name] = $value;
				}
			}
			if (!empty($retained_variables)) {
				cache_set('ajax_session_retain_'.$account->uid,'cache', serialize($retained_variables));
			}
			break;
		default:
			break;
	}
}
/**
 * implementation of hook_perm
 */
function ajax_session_perm() {
	return array('access ajax session');
}

/**
 * implementation of hook_menu
 */
function ajax_session_menu() {
	if (!$may_cache) {
		drupal_add_js("path = 'http://".$_SERVER['HTTP_HOST'].base_path()."?q=ajax_session';","inline");
		drupal_add_js("sess = '"._get_session_id()."';","inline");
		drupal_add_js(drupal_get_path('module', 'ajax_session') .'/ajax_session.js');
    $items[] = array(
      'path' => 'ajax_session',
      'callback' => 'set_ajax_session_variable',
      'access' => user_access('access ajax session'),
      'type' => MENU_CALLBACK
		);
	}
	return $items;
}
 
/**
 * Register a session variable that is safe to use with AJAX session 
 * @param
 * $variable: The name of session variable to be registered
 * $retain: Whether the value of the variable will be retained across user sessions
 */
function ajax_session_register($variable,$retain=FALSE) {
	$variables = variable_get('ajax_session_safe_variables',array());
	$retained = variable_get('ajax_session_retained',array());
	
	if (!in_array($variable,$variables)) {
		$variables[] = $variable;
		variable_set('ajax_session_safe_variables',$variables);
		if ($retain) {
			$retained[] = $variable;
			variable_set('ajax_session_retained',$retained);
		}
	}
}

/**
 * Unregister a session variable that is safe to use with AJAX session 
 * @param
 * $variable: The name of session variable to be unregistered
 */
function ajax_session_unregister($variable) {
	$variables = variable_get('ajax_session_safe_variables',array());
	$retained = variable_get('ajax_session_retained',array());
	foreach ($variables as $index => $value) {
		if ($value == $variable) {
			unset($variables[$index]);
		}
	}
	foreach ($retained as $index => $value) {
		if ($value == $variable) {
			unset($retained[$index]);
		}
	}
	variable_set('ajax_session_safe_variables',$variables);
	variable_set('ajax_session_retained',$retained);
}

/**
* The main function of AJAX session is a menu callback that will used with a 
* HTTP POST through AJAX. The POST request checks the submitted session id to see
* if the user agent IP address that submitted the request corresponds to the session.
* If that check passes, each variable is checked to see if it is on a white list.
* If it is, the variable (and its value) is stored in the cache and will be set
* in the user session variable when that user-agent requests a new page.
*/
function set_ajax_session_variable() {
	$safe_variables = variable_get('ajax_session_safe_variables',array());
	$stored_variables = array(); 
	
  // HTTP post & session ID & valid variables required
	if (is_null($_POST['sid']) || $_SERVER['REQUEST_METHOD'] != 'POST' || empty($safe_variables)) {
		print 'req';
		exit;
	}
	if ($_SERVER['REMOTE_ADDR'] != db_result(db_query("SELECT hostname FROM {sessions} WHERE sid = '%s'",$_POST['sid']))) {
		print 'ip';
		exit;
	}
	foreach ($_POST as $name => $value) {
		if ($name != 'sid' && in_array($name,$safe_variables)) { 
			$stored_variables[$name] = $value;
		}
	}
	
	cache_set('ajax_session_'.$_POST['sid'],'cache', serialize($stored_variables),CACHE_TEMPORARY);
	exit;
}

/**
 * Return a session ID for user
 * @return
 * A string containing the session ID, return FALSE if not found. 
 */
function _get_session_id() {
	foreach ($_COOKIE as $name => $value) {
		if (substr($name,0,4) == "SESS") {
			return $value;
		}
	}
	return FALSE;
}
